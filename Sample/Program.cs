﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntlibCacheStorage;
using System.Threading;

namespace Sample
{
    class Program
    {
        static ICache cache = new CacheImpl("Cache Manager");
        static void Main(string[] args)
        {
            for (int i = 0; i < 100; i++)
            {
                var str = GetData("aaaa");
                Console.WriteLine(str);
                Thread.Sleep(5000);
            }
            Console.WriteLine("end");
            Console.ReadKey();
        }

        static string GetData(string key)
        {
            string result = cache.Get<string>(key);
            if (result == null)
            {
                result = new Random().Next().ToString();
                cache.Set<string>(key, result, TimeSpan.FromSeconds(10));
            }
            return result;
        }

    }
}
