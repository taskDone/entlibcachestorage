﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NLog;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace EntlibCacheStorage
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class CacheItemRefreshAction : ICacheItemRefreshAction
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        [NonSerialized]
        private ICacheManager _cacheManager;

        #region Ctor
        public CacheItemRefreshAction(string cacheManagerName)
            : this(CacheFactory.GetCacheManager(cacheManagerName))
        {

        }
        public CacheItemRefreshAction(ICacheManager cacheManager)
        {
            if (cacheManager == null)
                throw new ArgumentNullException("cacheManager");

            _cacheManager = cacheManager;
        }
        #endregion

        #region ICacheItemRefreshAction
        public void Refresh(string removedKey, object expiredValue, CacheItemRemovedReason removalReason)
        {
            if (CacheItemRemovedReason.Expired == removalReason)
            {
                _cacheManager.Remove(removedKey);
            }
        }
        #endregion
    }

}
