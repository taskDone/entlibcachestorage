﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using System.Collections;

namespace EntlibCacheStorage
{
    public interface ICacheDataStore
    {
        /// <summary>
        /// add cache
        /// </summary>
        /// <param name="partitionName"></param>
        /// <param name="cacheItem"></param>
        void Add(string partitionName, CacheItem cacheItem);

        /// <summary>
        /// Count
        /// </summary>
        /// <param name="partitionName"></param>
        /// <returns></returns>
        int Count(string partitionName);

        /// <summary>
        /// load the cache
        /// </summary>
        /// <param name="partitionName"></param>
        /// <returns></returns>
        Hashtable Load(string partitionName);

        /// <summary>
        /// remove
        /// </summary>
        /// <param name="partitionName"></param>
        /// <param name="storageKey"></param>
        /// <returns></returns>
        int Remove(string partitionName, int storageKey);

        /// <summary>
        /// truncate
        /// </summary>
        /// <param name="partitionName"></param>
        void Truncate(string partitionName);
    }
}
