﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntlibCacheStorage
{
    /// <summary>
    /// Cache Interface
    /// </summary>
    public interface ICache
    {
        /// <summary>
        /// 缓存数量 
        /// </summary>
        int Count { get; }

        /// <summary>
        /// 清空缓存
        /// </summary>
        void Clear();

        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Contains(string key);

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        T Get<T>(string key);

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        bool TryGet<T>(string key, out T value);

        /// <summary>
        /// 设置缓存 失效日期采用默认值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        void Set<T>(string key, T value);

        /// <summary>
        /// 设置缓存，失效日期为某个时间点
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="absoluteExpiration"></param>
        void Set<T>(string key, T value, DateTime absoluteExpiration);

        /// <summary>
        /// 设置缓存，过多长时间失效
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="slidingExpiration"></param>
        void Set<T>(string key, T value, TimeSpan slidingExpiration);

        /// <summary>
        /// 移除某个缓存
        /// </summary>
        /// <param name="key"></param>
        void Remove(string key);
    }
}
